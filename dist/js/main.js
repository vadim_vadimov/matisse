function initMap() {
        // var map = new google.maps.Map(document.getElementById('map'), {
        //   zoom: 13,
        //   center: {lat: 43.5164259, lng: 39.8174274},
        //   disableDefaultUI: true
        // });

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 17,
          center: {lat: 43.576589, lng: 39.725671},
          disableDefaultUI: true,
          zoomControl: true,
        });

        var map2 = new google.maps.Map(document.getElementById('map-2'), {
          zoom: 16,
            center: {lat: 43.576589, lng: 39.725671},
          disableDefaultUI: true,
          zoomControl: true,
        });

        var marker = new google.maps.Marker({
              position: {lat: 43.576589, lng: 39.725671},
              icon: 'img/map-geo.png',
              map: map,
            });
        //
        // var marker2 = new google.maps.Marker({
        //       position: {lat: 43.4774541, lng: 39.8933742},
        //       icon: 'img/loc-1.png',
        //       map: map2
        //     });
        //
        //
        // var marker3 = new google.maps.Marker({
        //       position: {lat: 43.4774211, lng: 39.8957701},
        //       icon: 'img/loc-2.png',
        //       map: map2
        //     });
        //
        // var marker4 = new google.maps.Marker({
        //       position: {lat: 43.477616, lng: 39.897635},
        //       icon: 'img/loc-3.png',
        //       map: map2
        //     });

        var marker5 = new google.maps.Marker({
              position: {lat: 43.576589, lng: 39.725671},
              icon: 'img/loc-4.png',
              map: map2
            }); 

        // var marker6 = new google.maps.Marker({
        //       position: {lat: 43.472796, lng: 39.896104},
        //       icon: 'img/loc-5.png',
        //       map: map2
        //     });
        //
        // var marker7 = new google.maps.Marker({
        //       position: {lat: 43.471426, lng: 39.894322},
        //       icon: 'img/loc-6.png',
        //       map: map2
        //     });
        //
        // var marker8 = new google.maps.Marker({
        //       position: {lat: 43.472181, lng: 39.897120},
        //       icon: 'img/loc-7.png',
        //       map: map2
        //     });
        //
        // var marker9 = new google.maps.Marker({
        //       position: {lat: 43.467011, lng: 39.895591},
        //       icon: 'img/loc-8.png',
        //       map: map2
        //     });

        var styles = 

        [
          {
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#333333"
              }
            ]
          },
          {
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#212121"
              }
            ]
          },
          {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "administrative.country",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          },
          {
            "featureType": "administrative.land_parcel",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "administrative.locality",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#bdbdbd"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#181818"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#1b1b1b"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#282828"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#8a8a8a"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#282828"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#282828"
              }
            ]
          },
          {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#4e4e4e"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "featureType": "transit",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#000000"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#2b2b2b"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#3d3d3d"
              }
            ]
          }
        ]

        map.setOptions({styles: styles});

      }



$(function () {

  if ($(window).width() > 1023) {

    $('.parallax-wrap').parallax({
      calibrateX: true,
      calibrateY: false,
      limitX: false,
      limitY: false,
      scalarX: 2,
      scalarY: 10,
      frictionX: 0.2,
      frictionY: 0.2
    });

  }

  $( ".interior__plus" ).hover(function() {
    $( this ).prev().toggleClass("active");
  });

  if($('.about__slider').length > 0) {
        $('.about__slider').slick({
            slidesToShow: 1,
            infinite: true,
            dots: false,
            arrows: false,
            speed: 700,
            slidesToScroll: 1,
        });
    }


  $(".about__slider-btn--prev").on("click", function() {
      $(".about__slider").slick("slickPrev");
  });

  $(".about__slider-btn--next").on("click", function() {
      $(".about__slider").slick("slickNext");
  });  


  $('.form-tel-name').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             tel: {
                 required: true
             },
             name: {
                 required: true
             },

         },
         messages: {
             tel: {
                 required: 'Укажите ваш телефон'
             },
             name: {
                 required: 'Укажите ваше имя'
             },
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "tel-name.php",
                 data: $form.serialize()
             }).done(function(e) {
                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 // window.location = "thanks-page.html"
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });

  
});